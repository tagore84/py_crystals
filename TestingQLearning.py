# From https://colab.research.google.com/github/ehennis/ReinforcementLearning/blob/master/05-DQN.ipynb#scrollTo=VKrubqPyLU4Z

from DeepQNetwork import DeepQNetwork
from py4j.java_gateway import JavaGateway

gateway = JavaGateway()


import numpy as np


#Global Variables

NUM_GAMES = 10000

#Hyper Parameters
def discount_rate(): #Gamma
    return 0.95

def learning_rate(): #Alpha
    return 0.001

def batch_size(): #Size of the batch used in the experience replay
    return 1000

#Create the agent
controller = gateway.entry_point
dimension_factories = (5 + 1) * 5
dimension_slot_boards = 15 + 7
dimension_final_board = 25
input_size = dimension_factories + ((dimension_slot_boards + dimension_final_board) * 2)
output_dimension = 180
epsilon = 1
epsilon_min = 0.05
epsilon_decay = 0.999

dqn = DeepQNetwork(input_size, output_dimension, learning_rate(), discount_rate(), epsilon, epsilon_min, epsilon_decay )

batch_size = batch_size()

#Training
rewards = [] #Store rewards for graphing
epsilons = [] # Store the Explore/Exploit
TEST_Episodes = 0
for game in range(NUM_GAMES):
    print("starting game " + str(game) + "...")
    controller.createGame("-seed " + str(game) + " -player HUMAN 0 Zanaorio_0 -player HUMAN 1 Zanaorio_1");
    state = controller.getGameState().toVector()
    print(controller.getGameState().toString())
    state = np.reshape(state, [1, input_size]) # Resize to store in memory to pass to .predict
    tot_rewards = 0
    while (True):
        action = dqn.action(state)
        reward = controller.move(action)
        if (reward != -10):
            print('.', end='')
        done = reward != 0;
        while (not controller.getGameState().getCurrentPlayer().isHuman()):
            print("wainting...")
        nstate = controller.getGameState().toVector()
        nstate = np.reshape(nstate, [1, input_size])
        tot_rewards += reward
        dqn.store(state, action, reward, nstate, done)
        state = nstate
        if (controller.getGameState().isFinish()):
            rewards.append(tot_rewards)
            epsilons.append(dqn.epsilon)
            print("episode: {}/{}, score: {}, e: {}"
                  .format(game, NUM_GAMES, tot_rewards, dqn.epsilon))
            break
            # Experience Replay
        if len(dqn.memory) > batch_size:
            dqn.experience_replay(batch_size)

dqn.model.save('first_model.h5')